import 'package:flutter/material.dart';
import 'package:upah/screen/logo_screen.dart';
import 'package:upah/screen/user/home_screen.dart';
import 'package:upah/screen/login_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Upah',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Logo_Screen(),
//      Home_Screen(),
//      Logo_Screen(),
//      Login_Screen(),
      debugShowCheckedModeBanner: false,
    );
  }
}