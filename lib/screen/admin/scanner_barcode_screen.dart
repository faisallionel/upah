import 'package:flutter/material.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/services.dart';
import 'package:flutter_camera_ml_vision/flutter_camera_ml_vision.dart';
import 'package:upah/ScannerOverlay.dart';

class Scanner_Barcode_Screen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _Scanner_Barcode_Screen();
}

class _Scanner_Barcode_Screen extends State<Scanner_Barcode_Screen>{
  bool resultSent = false;
  List<String> data = [];

  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          CameraMlVision<List<Barcode>>(
            overlayBuilder: (c) {
              return Container(
                decoration: ShapeDecoration(
                  shape: ScannerOverlayShape(
                    borderColor: Theme.of(context).primaryColor,
                    borderWidth: 3.0,
                  ),
                ),
              );
            },
            detector: FirebaseVision.instance.barcodeDetector().detectInImage,
            onResult: (List<Barcode> barcodes) {
              if (!mounted || resultSent) {
                return;
              }
              resultSent = true;
              print(barcodes.first);
              //ngisine ndek kene
              Navigator.of(context).pop<Barcode>(barcodes.first);
            },
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height / 4,
            color: Colors.blue,
            child: Center(child: Text('Upah')),
          ),
        ],
      ),
    );
  }
}