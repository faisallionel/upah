import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:country_pickers/country_pickers.dart';
import 'package:country_pickers/country.dart';
import 'package:upah/screen/user/home_screen.dart';
import 'package:http/http.dart' as http;

class Login_Screen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _Login_Screen();
}

class _Login_Screen extends State<Login_Screen>{

  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  TextEditingController phoneNumeberCtrl = TextEditingController();

  Country _selectedDialogCountry = CountryPickerUtils.getCountryByPhoneCode('62');


  @override
  Widget build(BuildContext context) {
    _checkUser();
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(15),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Image.asset(
                'assets/login.png'
              ),
              Text('Masukan nomor hp dengan benar'),
              Container(
                decoration: new BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                    color: Colors.black26,
                      width: 1.0
                    ),
                    borderRadius: new BorderRadius.circular(10.0)
                ),
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: Container(
                        child: InkWell(
                          child: _buildDialogItem(_selectedDialogCountry),
                          onTap: _openCountryPickerDialog,
                        ),
                      ),
                    ),
                    Flexible(
                      child: TextFormField(
                        controller: phoneNumeberCtrl,
                        style: TextStyle(fontSize: 20),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: '8XXXXXXXXXXX2',
                          hintStyle: TextStyle(fontSize: 20),
                          contentPadding: EdgeInsets.symmetric(horizontal: 15),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: double.infinity,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)
                  ),
                  color: Color(0xFF1783FF),
                  padding: const EdgeInsets.all(14.0),
                  child: Text(
                    'Lanjutkan',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 18
                    ),
                  ),
                  onPressed: (){
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => Login_Screen()));
                  },
                ),
              ),
              Container(
                padding: const EdgeInsets.all(10),
                decoration: new BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                        color: Colors.black26,
                        width: 1.0
                    ),
                    borderRadius: new BorderRadius.circular(10.0)
                ),
                child: InkWell(
                  child: Row(
                    //crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 25),
                        child: Image.asset(
                          'assets/google.png',
                          width: 25,
                        ),
                      ),
                      Text(
                        'Sign in with Google',
                        style:  TextStyle(
                          fontSize: 18
                        ),
                      )
                    ],
                  ),
                  onTap: Signin_Google,
                ),
              ),
//              Container(
//                padding: const EdgeInsets.all(10),
//                decoration: new BoxDecoration(
//                    color: Colors.white,
//                    border: Border.all(
//                        color: Colors.black26,
//                        width: 1.0
//                    ),
//                    borderRadius: new BorderRadius.circular(10.0)
//                ),
//                child: InkWell(
//                  child: Row(
//                    //crossAxisAlignment: CrossAxisAlignment.center,
//                    children: <Widget>[
//                      Padding(
//                        padding: const EdgeInsets.symmetric(horizontal: 25),
//                        child: Image.asset(
//                          'assets/google.png',
//                          width: 25,
//                        ),
//                      ),
//                      Text(
//                        'LOGOUT',
//                        style:  TextStyle(
//                            fontSize: 18
//                        ),
//                      )
//                    ],
//                  ),
//                  onTap: _handleSignOut,
//                ),
//              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildDialogItemList(Country country) => Row(
    children: <Widget>[
      CountryPickerUtils.getDefaultFlagImage(country),
      SizedBox(width: 8.0),
      Text("+${country.phoneCode}"),
      SizedBox(width: 8.0),
      Flexible(child: Text(country.name))
    ],
  );

  Widget _buildDialogItem(Country country) => Row(
    children: <Widget>[
      Text(
        "+${country.phoneCode}",
        style: TextStyle(
          fontSize: 18
        ),
      ),
      SizedBox(width: 8),
      Icon(Icons.arrow_drop_down),
    ],
  );

  void _openCountryPickerDialog() => showDialog(
    context: context,
    builder: (context) => Theme(
      data: Theme.of(context).copyWith(primaryColor: Colors.blue),
      child: CountryPickerDialog(
        titlePadding: EdgeInsets.all(8.0),
        searchCursorColor: Colors.blueAccent,
        searchInputDecoration: InputDecoration(hintText: 'Search...'),
        isSearchable: true,
        title: Text('Select your phone code'),
        onValuePicked: (Country country) =>
            setState(() => _selectedDialogCountry = country),
        itemBuilder: _buildDialogItemList,
      ),
    ),
  );

  Future<void> _handleSignOut() async {
    print('berhasil logout');
    _googleSignIn.disconnect();
    await _auth.signOut();
  }

  void _checkUser() async {
    final FirebaseUser user = await _auth.currentUser();
    if (user == null) {
      print("tidak ada user");
    } else {
      Navigator.pushReplacement(context, new MaterialPageRoute(builder: (context) => Home_Screen()));
    }
  }

  void Signin_Google()async{
    final GoogleSignInAccount account = await _googleSignIn.signIn();
    final GoogleSignInAuthentication signInAuthentication = await account.authentication;

    final AuthCredential  credential = GoogleAuthProvider.getCredential(idToken: signInAuthentication.idToken, accessToken: signInAuthentication.accessToken);

    final FirebaseUser user = (await _auth.signInWithCredential(credential)).user;
    print('email = ${user.email}');

//    access api on server upah

    var client = http.Client();
    try {
      var response = await client.post('http://61.8.68.229:3000/api/user/signup', body: {
        'email': '${user.email}',
        'password': '${user.email}',
        'nama': '${user.displayName}',
        'telp': '${user.phoneNumber}',
      });
      Navigator.pushReplacement(context, new MaterialPageRoute(builder: (context) => Home_Screen()));
      print('Response body: ${response.body}');
    } finally {
      client.close();
    }
  }
}