import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:upah/screen/start_screen.dart';

class Logo_Screen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _Logo_Screen();
}

class _Logo_Screen extends State<Logo_Screen> with TickerProviderStateMixin{
  AnimationController _animationControllerLogo;
  Animation<double> animationLogo;

  @override
  void initState() {
    super.initState();
    Animation_Logo();
  }

  @override
  void dispose() {
    super.dispose();
    _animationControllerLogo.dispose();
  }

  void Animation_Logo(){
    _animationControllerLogo = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this
    );
    animationLogo = CurvedAnimation(parent: _animationControllerLogo, curve: Curves.easeIn);
    _animationControllerLogo.forward();
    Timer(Duration(seconds: 5),() =>
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => Start_Screen())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Color(0xFF1783FF),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FadeTransition(
              opacity: animationLogo,
              child: SvgPicture.asset(
                'assets/logo.svg',
                height: MediaQuery.of(context).size.height / 4,
              ),
            ),
          ],
        ),
      ),
    );
  }
}