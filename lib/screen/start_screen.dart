import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:upah/screen/login_screen.dart';
import 'package:upah/screen/logo_screen.dart';
import 'package:upah/screen/user/home_screen.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:upah/screen/admin/scanner_barcode_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class Start_Screen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _Start_Screen();
}

class _Start_Screen extends State<Start_Screen>{

  String hello;
  final FirebaseAuth _auth = FirebaseAuth.instance;

  void _checkUser() async {
    final FirebaseUser user = await _auth.currentUser();
    if (user == null) {
      print("tidak ada user");
    } else {
      Navigator.pushReplacement(context, new MaterialPageRoute(builder: (context) => Home_Screen()));
    }
  }

  @override
  Widget build(BuildContext context) {
    _checkUser();
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color(0xFF78B7FF),
              Color(0xFF0069E2)
            ],
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SvgPicture.asset(
              'assets/illustration.svg',
              height: MediaQuery.of(context).size.height / 1.6,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Text(
                'Get as much money and rewards maybe in this application. Money and Reward is not our goal, environmental cleanliness is our reponsibility.',
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
              child: SizedBox(
                width: double.infinity,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)
                  ),
                  color: Colors.white,
                  padding: const EdgeInsets.all(12.0),
                  child: Text(
                    'Mulai',
                    style: TextStyle(
                      color: Color(0xFF1783FF),
                      fontWeight: FontWeight.bold,
                      fontSize: 18
                    ),
                  ),
                  onPressed: (){
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => Login_Screen()));
//                    final barcode = await Navigator.of(context).push<Barcode>(
//                      MaterialPageRoute(builder: (c) {
//                        return Scanner_Barcode_Screen();
//                      },
//                      ),
//                    );
//                    if (barcode == null) {
//                      return;
//                    }
//                    setState(() {
//                      hello = barcode.displayValue;
//                      print('data : $hello');
//                    });
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}