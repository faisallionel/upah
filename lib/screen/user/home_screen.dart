import 'dart:math';
import 'package:fl_chart/fl_chart.dart';
import 'package:upah/screen/login_screen.dart';
import 'indicator.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

Size deviceSize;
double height;
double width;

class Home_Screen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _Home_Screen();
}

class _Home_Screen extends State<Home_Screen>{
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  String username = "";
  int touchedIndex;

  Widget charts() {
    return AspectRatio(
      aspectRatio: 3.3,
      child: Card(
        color: Colors.white,
        child: Row(
          children: <Widget>[
            const SizedBox(
              height: 18,
            ),
            Expanded(
              child: AspectRatio(
                aspectRatio: 1,
                child: PieChart(
                  PieChartData(
                      pieTouchData: PieTouchData(touchCallback: (pieTouchResponse) {
                        setState(() {
                          if (pieTouchResponse.touchInput is FlLongPressEnd ||
                              pieTouchResponse.touchInput is FlPanEnd) {
                            touchedIndex = -1;
                          } else {
                            touchedIndex = pieTouchResponse.touchedSectionIndex;
                          }
                        });
                      }),
                      borderData: FlBorderData(
                        show: false,
                      ),
                      sectionsSpace: 0,
                      centerSpaceRadius: 40,
                      sections: showingSections()),
                ),
              ),
            ),
            Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const <Widget>[
                Indicator(
                  color: Color(0xff0293ee),
                  text: 'First',
                  isSquare: true,
                ),
                SizedBox(
                  height: 4,
                ),
                Indicator(
                  color: Color(0xfff8b250),
                  text: 'Second',
                  isSquare: true,
                ),
                SizedBox(
                  height: 4,
                ),
                Indicator(
                  color: Color(0xff845bef),
                  text: 'Third',
                  isSquare: true,
                ),
                SizedBox(
                  height: 4,
                ),
                Indicator(
                  color: Color(0xff13d38e),
                  text: 'Fourth',
                  isSquare: true,
                ),
                SizedBox(
                  height: 18,
                ),
              ],
            ),
            const SizedBox(
              width: 28,
            ),
          ],
        ),
      ),
    );
  }

  List<PieChartSectionData> showingSections() {
    return List.generate(4, (i) {
      final isTouched = i == touchedIndex;
      final double fontSize = isTouched ? 25 : 16;
      final double radius = isTouched ? 60 : 50;
      switch (i) {
        case 0:
          return PieChartSectionData(
            color: const Color(0xff0293ee),
            value: 40,
            title: '40%',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize, fontWeight: FontWeight.bold, color: const Color(0xffffffff)),
          );
        case 1:
          return PieChartSectionData(
            color: const Color(0xfff8b250),
            value: 30,
            title: '30%',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize, fontWeight: FontWeight.bold, color: const Color(0xffffffff)),
          );
        case 2:
          return PieChartSectionData(
            color: const Color(0xff845bef),
            value: 15,
            title: '15%',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize, fontWeight: FontWeight.bold, color: const Color(0xffffffff)),
          );
        case 3:
          return PieChartSectionData(
            color: const Color(0xff13d38e),
            value: 15,
            title: '15%',
            radius: radius,
            titleStyle: TextStyle(
                fontSize: fontSize, fontWeight: FontWeight.bold, color: const Color(0xffffffff)),
          );
        default:
          return null;
      }
    });
  }
  _getUserAuth(){
    FirebaseAuth.instance.currentUser().then((user){
      setState(() {
        var uname = user.displayName;
        if(uname.split(" ").length > 2) {
          uname = uname.split(" ")[0].toString() + " " + uname.split(" ")[1].toString() + " " + uname.split(" ")[2].toString();
        } else {
          uname = user.displayName;
        }
        this.username = uname;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    _getUserAuth();
    deviceSize = MediaQuery.of(context).size;
    height = deviceSize.height;
    width = deviceSize.width;

    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter ,
            end: Alignment.bottomCenter,
            colors: [
              Color(0xFFEDF6FF),
              Color(0xFFFFFFFF),
            ],
          ),
        ),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 30.0, left: 15.0, right: 15.0),
              child: Upper_Content(),
            ),
            Row(
              children: <Widget>[
                Container(
                  width: 360.0,
                  height: 300.0,
                  child: charts(),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget Upper_Content(){  // konten nama dan burger menu
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          'Hai, ${this.username}',
          style: TextStyle(
            fontSize: 20,
            color: Colors.black54
          ),
        ),
        PopupMenuButton(
          itemBuilder: (BuildContext context) => <PopupMenuEntry>[
            PopupMenuItem(
              value: 1,
              child: Row(
                children: <Widget>[
                  Icon(Icons.info),
                  SizedBox(width: 20.0,),
                  Text('Info')
                ],
              ),
            ),
            PopupMenuItem(
              value: 2,
              child: Row(
                children: <Widget>[
                  Icon(Icons.settings),
                  SizedBox(width: 20.0,),
                  Text('Setting')
                ],
              ),
            ),
            PopupMenuItem(
              value: 3,
              child: Row(
                children: <Widget>[
                  Icon(Icons.exit_to_app),
                  SizedBox(width: 20.0,),
                  Text('Logout')
                ],
              ),
            ),
          ],
          elevation: 4,
          onCanceled: () {
            print("You have canceled the menu.");
          },
          tooltip: "Opsi Lainnya",
          onSelected: (value) async {
            if(value == 1){

            } else if(value == 2){

            } else if(value == 3) {
              _googleSignIn.disconnect();
              await _auth.signOut();
              Navigator.pushReplacement(context, new MaterialPageRoute(builder: (context) => Login_Screen()));
            }
          },
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.white
            ),
            padding: const EdgeInsets.all(10),
            child: Icon(
              Icons.menu,
              color: Colors.green,
              size: 30,
            ),
          ),
          offset: Offset(0, 100),
        )
      ],
    );
  }
}

